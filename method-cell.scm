(define-module (method-cell)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:export (^cell))

(define (^cell bcom val)
  (methods            ; syntax for first-argument-symbol-based dispatch
    ((get)            ; takes no arguments
      val)            ; returns current value
    ((set new-val)    ; takes one argument, new-val
      (bcom (^cell bcom new-val  ))))) ; become a cell with the new value
